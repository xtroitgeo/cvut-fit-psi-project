from ClientServerMessages import *


def parseData(message, connection, stage):

    message = message.decode('utf-8')
    pos = 0
    dataOfMessages = ''
    listMessages = []

    print('First part :', message)
    print('Length message: ', len(message))

    if message.find('\a\b') == -1 and not checkLength(message, stage):
        return message, 0

    while message[len(message) - 2] != '\a' and message[len(message) - 1] != '\b':


        if '\0' in message:
            break

        if message.find('\a\b') == -1 and maxLen(message, stage):
            return message, 0

        if not checkLength(message, stage) and message.find('\a\b') == -1:
            return message, 0


        connection.settimeout(TIMEOUT)
        print('Message before append: ', message)
        newMessage = connection.recv(BUFFER_SIZE)
        newMessage = newMessage.decode('utf-8')
        print('NEW message: ', newMessage)
        message += newMessage
        print('Message AFTER MERGING: ', message)


    while True:
        if message[pos] == '\a' and message[pos+1] == '\b':
            if pos == len(message) - 2:
                return message, 0
            else:
                additionalMessages = ''
                listMessages.append(dataOfMessages)
                pos += 2
                while True:
                    if message[pos] == '\a' and message[pos+1] == '\b':
                        if pos == len(message) - 2:
                            listMessages.append(additionalMessages)
                            return listMessages, len(listMessages)
                        else:
                            listMessages.append(additionalMessages)
                            additionalMessages = ''
                            pos += 2
                    else:
                        additionalMessages += message[pos]
                        pos += 1

        else:
            dataOfMessages += message[pos]
            pos += 1


def getServerHash(message, name):

    nameSum = 0
    serverHash = 0

    if not message[:-2].isnumeric():
        return -403, 0, 0

    key = int(message[0])

    if key > 4 or key < 0:
        return -404, 0, 0

    serverKey = KEYS.get(key)[0]
    clientKey = KEYS.get(key)[1]


    for char in name[:-2]:
        nameSum += ord(char)

    nameSum *= 1000
    serverHash = nameSum % 65536
    nameSum += serverKey
    nameSum %= 65536
    nameSum = str(nameSum)
    nameSum += '\a\b'

    return nameSum, serverHash, clientKey


def compareHash(serverHash, clientHash, clientKey):

    serverHash = int(serverHash)
    clientHash = clientHash[:-2]
    clientHash = int(clientHash)
    clientHash = (clientHash - clientKey) % 65536

    if serverHash == clientHash:
        return True

    return False



def checkLength(message, stage):

    messageLen = len(message)

    if 'RECHRAGING' in message or 'FULL POWER' in message:
        return messageLen <= CLIENT_FULL_POWER_LEN

    if stage == Stages.GET_USERNAME:
        return messageLen <= CLIENT_USERNAME_LEN

    elif stage == Stages.GET_KEY:
        return messageLen <= CLIENT_KEY_ID_LEN

    elif stage == Stages.CHECK_HASH:
        return messageLen <= CLIENT_CONFIRMATION_LEN

    elif stage == Stages.FIRST_MOVE or stage == Stages.SECOND_MOVE:
        return messageLen <= CLIENT_OK_LEN

    return messageLen <= CLIENT_MESSAGE_LEN


def maxLen (message, stage):

    messageLen = len(message)

    if 'RECHRAGING' in message or 'FULL POWER' in message:
        return messageLen == CLIENT_FULL_POWER_LEN

    if stage == Stages.GET_USERNAME:
        return messageLen == CLIENT_USERNAME_LEN

    elif stage == Stages.GET_KEY:
        return messageLen == CLIENT_KEY_ID_LEN

    elif stage == Stages.CHECK_HASH:
        return messageLen == CLIENT_CONFIRMATION_LEN

    elif stage == Stages.FIRST_MOVE or stage == Stages.SECOND_MOVE:
        return messageLen == CLIENT_OK_LEN

    return messageLen == CLIENT_MESSAGE_LEN


