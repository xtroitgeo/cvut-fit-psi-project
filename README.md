# BI-PSI - Computer Networks
# Project 
[Assignment](assignment.pdf)  in Czech.

### Task details
- Server for automatic remote control of robots
- The robots log in to the server and it guides them to the center of the coordinate system
- At the destination coordinate, the robot must pick up a secret. On the way to the goal, the robots may encounter
obstacles that the robots must avoid
- The server can navigate multiple robots at the same timey and implements faultless communication protocol
- The communication between the server and the robots is implemented by a fully text-based protocol
- Each command ends by a pair of special symbols "\a\b"


### Run:
Server: python3 main.py \
Client:
- run **Client.ova** 
- tester <port> <server address> 

### Evaluation: full score

