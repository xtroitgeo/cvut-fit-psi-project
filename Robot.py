from ClientServerMessages import Orientation


class Robot:

    def __int__(self):
        self.x = 0
        self.y = 0
        self.oldX = 0
        self.odY = 0
        self.Orientation = 0

    x = 0
    oldX = 0
    y = 0
    oldY = 0

    def parseCoordinates(self, message):

        message = str(message)
        x = 0
        y = 0

        if message.find('.') != -1:
            return 'FLOAT', 0

        coordinates = message.split(' ')
        x = int(coordinates[1])
        y = int(coordinates[2][:-2])

        self.x = x
        self.y = y

        return x, y

    def setOldCoordinates(self, x, y):
        self.oldX = x
        self.oldY = y

    def isEqual(self, x, y):
        return self.oldX == x and self.oldY == y

    def robotWay(self, newX, newY):

        if self.oldX < newX:
            self.Orientation = Orientation.RIGHT
            return Orientation.RIGHT

        if self.oldX > newX:
            self.Orientation = Orientation.LEFT
            return Orientation.LEFT

        if self.oldY < newY:
            self.Orientation = Orientation.UP
            return Orientation.UP

        if self.oldY > newY:
            self.Orientation = Orientation.DOWN
            return Orientation.DOWN

        else:
            self.Orientation = Orientation.UNDEF

    def changeDirection(self):

        if atTarget(self.x, self.y):
            return False

        if self.x > 0 and self.Orientation == Orientation.LEFT:
            return False

        if self.x < 0 and self.Orientation == Orientation.RIGHT:
            return False

        if self.y > 0 and self.Orientation == Orientation.DOWN:
            return False

        if self.y < 0 and self.Orientation == Orientation.UP:
            return False

        return True


    def changeOrientation(self):

        if self.Orientation == Orientation.UP:
             self.Orientation = Orientation.RIGHT
        elif self.Orientation == Orientation.RIGHT:
             self.Orientation = Orientation.DOWN
        elif self.Orientation == Orientation.DOWN:
             self.Orientation = Orientation.LEFT
        elif self.Orientation == Orientation.LEFT:
             self.Orientation = Orientation.UP



    def sameCoordinates(self):
        return self.x == self.oldX and self.y == self.oldY

def atTarget(x, y):
    return x == 0 and y == 0
