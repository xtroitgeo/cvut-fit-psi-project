import socket
from threading import Thread
from ParseData import *
from ClientServerMessages import *
from Robot import *


class Server:

    def getConnection(self, serverSocket):
        while True:
            print('Waiting for a connection')
            connection, clientAddress = serverSocket.accept()
            connection.settimeout(TIMEOUT)
            thread1 = Thread(target=self.startNewThread, args=(connection, clientAddress))
            thread1.start()

    def startNewThread(self, connection, clientAddress):

        message = ''
        oneMessage = ''
        serverHash = ''
        clientName = ''
        SERVER_CONFIRMATION = ''
        clientKey = 0
        listOfMessages = []
        listIterator = 0
        listSize = 0
        hasSecondMessage = False
        isRecharging = False
        hasToSend = False
        hasToMove = True
        multipleMoves = False
        OvertakeObs = False
        stage = Stages.GET_USERNAME
        wall = ObstaclesTurns.RIGHT
        robot = Robot()

        try:
            print('Accepted connection ', clientAddress)

            while True:
                if not hasSecondMessage:
                    message = connection.recv(BUFFER_SIZE)
                    print('--------------')
                    print("Get message: ", message)
                    listOfMessages, listSize = parseData(message, connection, stage)

                if listSize != 0:
                    hasSecondMessage = True
                    oneMessage = listOfMessages[listIterator]
                    oneMessage += '\a\b'
                    listIterator += 1
                    listSize -= 1
                    if not listSize:
                        listIterator = 0
                        hasSecondMessage = False

                else:
                    oneMessage = listOfMessages

                ####################################################################################
                if 'RECHARGING' in oneMessage:
                    connection.settimeout(TIMEOUT_RECHARGING)
                    isRecharging = True
                    continue
                ####################################################################################
                if 'FULL POWER' in oneMessage:
                    isRecharging = False
                    connection.settimeout(TIMEOUT)
                    continue
                ####################################################################################
                if isRecharging:
                    connection.sendall(SERVER_LOGIC_ERROR.encode())
                    break
                ####################################################################################
                if stage == Stages.GET_USERNAME:
                    clientName = oneMessage

                    if len(clientName) > CLIENT_USERNAME_LEN or '\a\b' not in clientName:
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT
                        continue

                    else:
                        stage = Stages.GET_KEY
                        connection.sendall(SERVER_KEY_REQUEST.encode())

                ####################################################################################
                elif stage == Stages.GET_KEY:
                    SERVER_CONFIRMATION, serverHash, clientKey = getServerHash(oneMessage, clientName)

                    # NaN
                    if SERVER_CONFIRMATION == -403:
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT
                        break

                    # Out of range
                    elif SERVER_CONFIRMATION == -404:
                        connection.sendall(SERVER_KEY_OUT_OF_RANGE_ERROR.encode())
                        stage = Stages.LOGOUT
                    else:
                        connection.sendall(SERVER_CONFIRMATION.encode())
                        stage = Stages.CHECK_HASH
                ####################################################################################
                elif stage == Stages.CHECK_HASH:

                    # Out of range
                    if len(oneMessage[:-2]) > 5 or oneMessage[4] == ' ':
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT

                    elif compareHash(serverHash, oneMessage, clientKey):
                        connection.sendall(SERVER_OK.encode())
                        connection.sendall(SERVER_MOVE.encode())
                        stage = Stages.FIRST_MOVE
                    else:
                        connection.sendall(SERVER_LOGIN_FAILED.encode())
                        break
                ####################################################################################
                elif stage == Stages.FIRST_MOVE:

                    if oneMessage[:-2][-1] == ' ' or '\a\b' not in oneMessage or len(oneMessage) > CLIENT_OK_LEN:
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT
                        break

                    x, y = robot.parseCoordinates(oneMessage)


                    if x == 'FLOAT':
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT
                    else:


                        robot.setOldCoordinates(x, y)

                        if hasToSend:
                            stage = stage.LOGOUT
                            hasToSend = False
                        else:
                            stage = Stages.SECOND_MOVE
                            hasToSend = False
                            print('SECOND_MOVE')
                            connection.sendall(SERVER_MOVE.encode())

                ####################################################################################
                elif stage == Stages.SECOND_MOVE:

                    if oneMessage[:-2][-1] == ' ' or '\a\b' not in oneMessage or len(oneMessage) > CLIENT_OK_LEN:
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT
                        break

                    x, y = robot.parseCoordinates(oneMessage)


                    if x == 'FLOAT':
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        stage = Stages.LOGOUT

                    else:
                        hasToSend = False

                        if atTarget(x, y):
                            hasToSend = True
                            print('GET MESSAGE')

                        if (not multipleMoves) or (not OvertakeObs and not multipleMoves):
                            print('OvetakeOBS = ', OvertakeObs)
                            print('FIRST WAY_')
                            robot.robotWay(x, y)

                        # Same coordinates after MOVE
                        if (robot.sameCoordinates() and hasToMove) or OvertakeObs:


                            robot.setOldCoordinates(x, y)
                            if wall == ObstaclesTurns.RIGHT:
                                OvertakeObs = True
                                multipleMoves = True
                                print('TURN ->')
                                wall = ObstaclesTurns.MOVE
                                connection.sendall(SERVER_TURN_RIGHT.encode())
                                continue
                            elif wall == ObstaclesTurns.MOVE:
                                print('MOVE OBSTR')
                                OvertakeObs = True
                                wall = ObstaclesTurns.LEFT
                                connection.sendall(SERVER_MOVE.encode())
                                continue
                            elif wall == ObstaclesTurns.LEFT:
                                print('TURN -> 2')
                                OvertakeObs = True
                                wall = ObstaclesTurns.MOVE2
                                connection.sendall(SERVER_TURN_LEFT.encode())
                                continue
                            elif wall == ObstaclesTurns.MOVE2:
                                print('MOVE 2 OBST')
                                OvertakeObs = False
                                multipleMoves = False
                                wall = ObstaclesTurns.RIGHT
                                connection.sendall(SERVER_MOVE.encode())
                                continue

                        hasToMove = True
                        robot.setOldCoordinates(x, y)

                        if hasToSend:
                            stage = Stages.LOGOUT
                            connection.sendall(SERVER_PICK_UP.encode())
                            continue
                        else:
                            stage = Stages.SECOND_MOVE

                        multipleMoves = True
                        if robot.changeDirection():
                            hasToMove = False
                            robot.changeOrientation()
                            print('TURN RIGHT')
                            connection.sendall(SERVER_TURN_RIGHT.encode())

                        if hasToMove:
                            print('MOVE')
                            connection.sendall(SERVER_MOVE.encode())

                ####################################################################################
                elif stage == Stages.LOGOUT:
                    if len(oneMessage) > CLIENT_MESSAGE_LEN or '\a\b' not in oneMessage:
                        connection.sendall(SERVER_SYNTAX_ERROR.encode())
                        break

                    else:
                        print('CLOSE SESSION')
                        connection.sendall(SERVER_LOGOUT.encode())
                        break

        except socket.timeout:
            print('Timeout connection')
        finally:
            connection.close()
