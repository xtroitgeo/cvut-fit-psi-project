import socket
from Server import *


def main():

    myServer = Server()
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverAddress = ('localhost', 3999)
    print('starting up on %s port %s' % serverAddress)
    serverSocket.bind(serverAddress)
    serverSocket.listen(1)

    myServer.getConnection(serverSocket)


if __name__ == '__main__':
    main()

